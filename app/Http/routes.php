<?php

// Route::get('/', 'PagesController@home');
// Route::get('about','PagesController@about');

Route::get('cards','CardsController@index');
Route::get('cards/{card}','CardsController@show');

Route::post('cards/{card}/notes','NotesController@store');

Route::get('/notes/{note}/edit','NotesController@edit'); //form or view to update UI for user
Route::patch('/notes/{note}','NotesController@update');  //action to update note