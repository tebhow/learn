<?php

namespace App\Http\Controllers;

use App\Card; 
use App\Note;
use Illuminate\Http\Request;
 
class NotesController extends Controller
{
   
	public function store(Request $request, Card $card)
	{
		// $note = new Note;
		// $note->body= $request->body;

		$card->addNote(
			new Note ($request->all())
			// when using request all don't forget to set which column in database that protected and fillable in database MODEL
		);
		
		return back();
	}

	public function edit(Note $note)
	{
		return view('notes.edit', compact('note'));
		// edit method to display the form for editing note. 
	}

	public function update(Request $request, Note $note)
	{
		$note->update($request->all());
		// When button in edit form triggered. This method running itself to update the note record in database. 

		return back();
	}
}
 