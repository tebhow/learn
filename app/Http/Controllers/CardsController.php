<?php

namespace App\Http\Controllers;

use App\Card;
use Illuminate\Http\Request;
use App\Http\Requests;

class CardsController extends Controller
{
    public function index()
    {
    	$cards = Card::all();

    	return view('cards.index', compact('cards'));
    }

    public function show(Card $card)
    {
    	//return $card->notes[0]->user; // n + 1 problems
    	//$card = Card::with('notes.user')->find(1); // eager loading 1st option
    	$card->load('notes.user');		// eager loading 2nd option

    	return view('cards.show', compact('card'));
    }
}
