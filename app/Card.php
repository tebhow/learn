<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
	public function notes()
	    {
	    	return $this->hasMany(Note::class);
	    	// $this refered to Cards Table
	    	// in other words (Card has many Notes) - one to many relationship tables
	    }    

	public function addNote(Note $note)
		{
			return $this->notes()->save($note);
		}
}
